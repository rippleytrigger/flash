<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'flash');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',aWT,],  UGx[b4N5-iM;LkP?~&C[qWzCm]~cvFzOBPxmrj):ef:au#47SM1&T+-');
define('SECURE_AUTH_KEY',  'Qw9;VmSRu63~uA)x4q0q>SC#aEz9{z>z43qEf6xa4Ed>aFgPpf9ddK0P&PQeOcEf');
define('LOGGED_IN_KEY',    ',nkIM~CUW:)~^nEdKIwYGMff+.vdg_?m6(8trWD!`/?5UwXl(/G?lz@}^dqD3V4c');
define('NONCE_KEY',        'boY|8$6l0._6y3Yh$s>2lg$?5(0)7RMq9kA0`H7Lot?R@oi@Ytj?X47}3]{!Z{a4');
define('AUTH_SALT',        'hhFRFiDV.Lpupzj~7~F|;OXG6#y3{crq_==N$>/LyvuUSF)i`OvN^t)E&t|@)|9G');
define('SECURE_AUTH_SALT', '.t_KMk` 58~I6iQy~wH=0*N@Y<3)D<6pyg_1-sy] iv$NXi,/03E&*Jrse>r 5&r');
define('LOGGED_IN_SALT',   ']9qd$zA&R;_vJGm(H#Z[{]__eUZ-,SU3[#a*uN1puWAJ;E`4_Y`z6%5F4KJ4AlSf');
define('NONCE_SALT',       'nnkLol]=jk$fK#yg5L~BI{S<W`byF{:ZC,.HMu@5]ZWONYl0t7=>X&4tf$w3uSM-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_fs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
 // Enable WP_DEBUG mode
 define( 'WP_DEBUG', true );
 
 // Enable Debug logging to the /wp-content/debug.log file
 define( 'WP_DEBUG_LOG', true );
 
 // Disable display of errors and warnings 
 define( 'WP_DEBUG_DISPLAY', false );
 @ini_set( 'display_errors', 0 );
 
 // Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
 define( 'SCRIPT_DEBUG', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
