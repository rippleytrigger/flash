<?php
    /*
    Template Name: Informar-Pago
    */ 
?>

<?php get_header('cms'); ?>

<div class="vc-composer-wrapper">
	<?php
		while ( have_posts() ) : the_post();

			the_content(); // Will output the content of visual composer

		endwhile; // End of the loop.
	?>
</div>
<?php get_footer() ?>
