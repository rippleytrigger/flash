/**
 * Author: Rippleytrigger <rippleytrigger@gmail.com>
 */


/** Functions **/
// Generic function to check everything is OK!
function listo() {
    console.log("main JS Loaded by @rippleytrigger");
}


function AgregarNombreArchivoInputFile(){
    document.querySelector('.file').addEventListener('change', function(){                           
        if(this.files != undefined) {
            document.querySelector('.btn-file-input').textContent = this.files[0].name;
        }else{
            document.querySelector('.btn-file-input').textContent = "Adjuntar Archivo";
        }
    })
}

function contactForm7(){
    let btnToggle = document.querySelector("#btn-solicitud-contact-form");
    let contactFormFlash = document.querySelector(".contact-form-flash");

    btnToggle.addEventListener("click", function() {
        contactFormFlash.classList.toggle("show");
        console.log(this);
        this.style.display = "none";
        
    })
}


function EfectojQueryScroll(event){
    
    let anchorHref = this.href; 
    //Se busca el Id del elemento a hacer scroll, en el href del elemento de navegacion
    let AnchorTargetIdLocation = anchorHref.search("#");
    if(AnchorTargetIdLocation != -1){

        //Se detiene el evento natural del anchor
        event.preventDefault();

        let AnchorTargetId = anchorHref.slice(AnchorTargetIdLocation)
        let scrollTargetLocation = jQuery(AnchorTargetId).offset();
        console.log(scrollTargetLocation)

        //Efecto Animate Scroll
        jQuery("html, body").animate({scrollTop: scrollTargetLocation.top}, 500);
    }else{
        console.log('El elemento no fue encontrado.')
    }
}

function EfectoScrollNavAnchors(targetMenuId){
    let menuAnchors = Array.from(document.querySelectorAll(`#${targetMenuId} a`));
    for(let anchors in menuAnchors){
        menuAnchors[anchors].addEventListener("click",EfectojQueryScroll);
    }
}

function EfectoScrollElementos(targetElement){
   
    let element = Array.from(document.querySelectorAll(targetElement));

    for(let iterador in element){
        element[iterador].addEventListener("click",EfectojQueryScroll);
    }
}

/** Loaders - Agrega tus scripts en esta seccion **/

// First Load - Puede que la pagina no este full cargada
jQuery(document).ready(function() {
    if(window.location.pathname === "/informar-pago"){
        AgregarNombreArchivoInputFile()
    }

    if(window.location.pathname === "/"){
        contactForm7();
        EfectoScrollNavAnchors("menu-nav-menu-left-1");
        EfectoScrollNavAnchors("menu-nav-menu-right-1");

        EfectoScrollElementos(".rippley-efecto-scroll");
    }
});

// Lazy load / La pagina cargo en su totalidad
jQuery(window).bind("load", function($){
    // Scripts a ejecutar
    listo();
});
