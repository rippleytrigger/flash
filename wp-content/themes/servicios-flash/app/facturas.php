<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

class wp_query_facturas{
    private $wpdb, $cliente, $facturas, $transacciones;

    public function __construct(){
        // Se almacena la variable wpdb, que permite realizar solicitudes de datos a la bd de wordpress
        global $wpdb;
        $this->wpdb = $wpdb;
    }

    public function __get($atributo){
        //Permite obtener un valor del objeto wpdb
        return $this->$atributo;
    }

    public function wp_query_response($repuesta){
        //Mensaje ejecutado cuando un query es realizado exitosamente
        $mensaje = "<div class='wpcf7-response-output wpcf7-mail-sent-ok'>";
        $mensaje .= $repuesta;
        $mensaje .= "</div>";

        echo $mensaje;
    }

    public function wp_query_error($error){
        $mensaje = "<div class='wpcf7-response-output wpcf7-validation-errors'>";
        $mensaje .= $error;
        $mensaje .= "</div>";

        echo $mensaje;
    }

    public function validar_correo($correo){
        //Validar Contenido del Input - Correo
        $correo = filter_var($correo, FILTER_SANITIZE_EMAIL);

        if($correo === false){
            $error = __("El formato con el que ingreso el correo es inválido", 'flash');
            $this->wp_query_error($error);
            return false;

        }else if($correo){
            return true;
        }
    }

    public function validar_input_numero($numero_factura){

        //Verifica que el numero de factura haya sido ingresado por el usuario
        if($numero_factura != NULL){

            //Validar Contenido del Input - Numero Factura
            $numero_factura = filter_var($numero_factura, FILTER_VALIDATE_INT);

            if($numero_factura === false){
                $error = __("El formato con el que ingreso el número de factura es inválido", 'flash');
                $this->wp_query_error($error);
                return false;

            }else{
                return true;
            }

        }else{
            return NULL;
        } 
    }

    public function wp_query_cliente(string $correo){

        $validacion_correo = $this->validar_correo($correo);

        if($validacion_correo != false && $validacion_correo != NULL){

            $wp_table_erp = "erp_peoples";
            $query = $this->wpdb->get_row( "SELECT * FROM {$this->wpdb->prefix}".$wp_table_erp.
            " WHERE email = '$correo'", ARRAY_A); 
    
            if($query != NULL){
                $this->cliente = $query;
            }else{
                $error = __("El correo ingresado no arrojo ningún resultado", 'flash');
                $this->wp_query_error($error);
            }
        }
    }

    public function wp_query_facturas($numero_factura){

       $validacion_numero = $this->validar_input_numero($numero_factura);

        //No se podra hacer ninguna busqueda en la tabla facturas, si no se haya ningun cliente.
        if($this->cliente != NULL && ($validacion_numero == true ||
        $validacion_numero == NULL)){


            $query = ($numero_factura != NULL)? 
            $this->cliente['id']." AND ref = '$numero_factura'" : $this->cliente['id'];


            $wp_table_erp = "erp_ac_transactions";
            $query = $this->wpdb->get_results( "SELECT * FROM {$this->wpdb->prefix}".$wp_table_erp.
            " WHERE user_id = ".$query." AND status = 'awaiting_payment'", ARRAY_A);

            if($query != NULL){
                $this->facturas = new stdClass;
                $this->facturas->results = $query;
                $this->facturas->rows = $this->wpdb->num_rows;
            }

            else if($query == NULL && $numero_factura != NULL){
                $error = __("El correo ingresado y número de factura ingresada 
                no arrojaron ningún resultado", 'flash');
                $this->wp_query_error($error);
            }
            
            else if($query == NULL && $numero_factura == NULL){
                $error = __("El correo ingresado no arrojó ningún resultado de factura
                pendiente", 'flash');
                $this->wp_query_error($error);
            }
        }
    }

    public function wp_query_transacciones($factura_id){
            //Evalua las transacciones de cada una de las facturas pendientes.
            $wp_table_erp = "erp_ac_transaction_items";

            $query = $this->wpdb->get_results( "SELECT * FROM {$this->wpdb->prefix}".$wp_table_erp.
            " WHERE transaction_id = ".$factura_id, ARRAY_A);
            
            if($query != NULL){
                $this->transacciones = new stdClass;
                $this->transacciones->results = $query;
                $this->transacciones->rows = $this->wpdb->num_rows;
            }else{
                $error = __('La factura pendiente no posee ninguna transaccion', 'flash');
                $this->wp_query_error($error);
            }
    }

    public function cambiar_status_cancelado(int $user_id){

        $wp_table_erp = "erp_ac_transactions";

        $this->wpdb->update( 
            "{$this->wpdb->prefix}".$wp_table_erp, 
            array( 
                'status' => 'paid',	// Estado Cancelado
                'due' => '0' //Disminucion del monto de la deuda
            ), 
            array( 'user_id' => $user_id, //Se usa el user id para obtener el registro cancelado 
                   'status' => 'awaiting_payment',
                   'form_type' => 'invoice' ), 
            array( 
                '%s',
            ), 
            array( '%s' ) 
        );
    }
}


class tabla_facturas{
    private $tabla, $erp_query, $moneda, $total, $status,
    $tabla_mostrada;


    public function __construct(wp_query_facturas $wp_query_facturas){
        $this->erp_query = $wp_query_facturas;

        $this->moneda = $this->erp_query->facturas->results[0]['currency'];

        //Esta propiedad toma lugar a la hora de imprimir la tabla 
        // Mide el total del monto pendiente de las facturas deudoras
        $this->total = 0;

        //Muestra el estado físico de la tabla
        $this->tabla_mostrada = false;
    }

    public function get_table_data($atributo){
        return $this->$atributo;
    }

    public function get_wpdb_data($atributo){
        return $this->erp_query->$atributo;
    }

    public function encolar_tabla(){

        //Se imprime el nombre del cliente
        $this->tabla = "<h3 class='texto-centrado'>".__('Cliente: ', 'flash') 
        ."{$this->erp_query->cliente['first_name']} {$this->erp_query->cliente['last_name']}</h3>";

            //Se inicializa la tabla
            $this->tabla .= "<div class='table-container'>";

            $this->tabla .= "<table class='rippley-tabla-facturas'>";
                $this->tabla .= "<tr class='headers'>";
                $this->tabla .= "<th class='numero-factura'>".__('N°', 'flash')."</th>";
                $this->tabla .= "<th>".__('Tipo', 'flash')."</th>";
                $this->tabla .= "<th>".__('N° de Factura', 'flash')."</th>";
                $this->tabla .= "<th>".__('Fecha de Emisión', 'flash')."</th>";
                $this->tabla .= "<th>".__('Fecha de Vencimiento', 'flash')."</th>";
                $this->tabla .= "<th>".__('Total', 'flash')."</th>";
                $this->tabla .= "<th>".__('Estado', 'flash')."</th>";
                $this->tabla .= "</tr>";
            
            for($i = 0; $i < $this->erp_query->facturas->rows; $i++){

                //Se reduce la cantidad de decimales del total de cada factura para una mejor visualización 
                $total = str_replace(".0000", ".00", $this->erp_query->facturas->results[$i]['total']);

                $this->tabla .= "<tr class='seccion-tabla'>";
                $this->tabla .= "<td class='numero-factura'>".($i+1)."</td>";

                $this->tabla .= "<td>". __('Factura', 'flash'). "</td>";

                $this->tabla .= "<td>".$this->erp_query->facturas->results[$i]['ref']."</td>";

                $this->tabla .= "<td>".$this->erp_query->facturas->results[$i]['issue_date']."</td>";
                $this->tabla .= "<td>".$this->erp_query->facturas->results[$i]['due_date']."</td>";
                $this->tabla .= "<td>".$total." ".$this->moneda."</td>";
                $this->tabla .= "<td>". __('Pendiente por pagar','flash')."</td>";
                $this->tabla .= "</tr>";

                //Se suma al total global de las facturas pendientes
                $this->total += $total;
            }
                
                //Se imprime el total de la suma de las facturas
                $this->tabla .= "<tr><td colspan='5'></td><td>".$this->total." ".
                $this->moneda."</td></tr>";

            $this->tabla .= "</table>";
            $this->tabla .= "</div>";

        if($this->erp_query->cliente != NULL && $this->erp_query->facturas != NULL){
            $this->tabla_mostrada = true;
            echo $this->tabla;
        }
    }
}


class pago_API{
    private $CCLW /*Código de Cliente Web*/;
    private $CMTN /*Monto de la transacción */;
    private $CDSC /*Descripcion de la transacción*/;
    private $erp_query,  $descripcion;

    public function __construct(wp_query_facturas $wpdb){
        $this->erp_query = $wpdb;
    }

    public function suministrar_datos_transaccion(string $CCLW,float $CMTN,string $CDSC){
        //Para ser usados en el link que redirigira al portal de paguelofacil
        $this->CCLW = $CCLW;
        $this->CMTN = $CMTN;
        $this->CDSC = $CDSC;
    }

    public function descripcion_facturas_pendientes(){

        //descripcion empleada para ser impresa en el get CDSC del API de paguelofacil
        $descripcion = __('Pago de Factura N° ', 'flash');

        for($i = 0; $i < $this->erp_query->facturas->rows; $i++){
        
            // Concatena Y
            if($i === $this->erp_query->facturas->rows - 1 && $this->erp_query->facturas->rows > 1){
                $descripcion .= __(' y ', 'flash');
            }

            //Concatena el numero de referencia de la factura
            $descripcion .= "{$this->erp_query->facturas->results[$i]['ref']}";

            // Concatena comas cuando son mas de tres facturas pendientes
            if($i < $this->erp_query->facturas->rows - 2 && $this->erp_query->facturas->rows > 2){
                $descripcion .= ', ';
            }
        }
        $descripcion .= '.';

        return $descripcion;
    }

    public function pago_realizado($get_transaccion, $get_usuario_id){
        //Se captura la respuesta tipo Get del API 
            $get_transaccion = $get_transaccion == 'aprovado' ? true : false;

            if($get_transaccion == true ){
                $this->erp_query->cambiar_status_cancelado($get_usuario_id);
                
                $mensaje = __('El pago se realizo', 'flash');
                $this->erp_query->wp_query_response($mensaje);
            }
            if($get_transaccion == false){
                $mensaje = __('El pago no pudo ser realizado', 'flash');
                $this->erp_query->wp_query_error($mensaje);
            }
    }

    public function imprimir(){
        $boton = "<a href='https://secure.paguelofacil.com/LinkDeamon.cfm?CCLW=$this->CCLW";
        $boton .= "&CMTN=$this->CMTN&CDSC=$this->CDSC'>";
        $boton .= "<img src='https://secure.paguelofacil.com/images/botones/Boton_De_Pago_1_100.png' 
        height='63' width='100' border='0' />";
        $boton .= "</a>";

        echo $boton;
    }
}


