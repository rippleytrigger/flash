<?php
    /*
    Template Name: Facturacion
    */ 
?>

<?php get_header('cms'); ?>

    <div class="vc-composer-wrapper">
        <?php
            while ( have_posts() ) : the_post();

                the_content(); // Will output the content of visual composer

            endwhile; // End of the loop.
        ?>
    </div>
    <div class="flash-seccion-facturacion">

    
    <?php //include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
<?php //is_plugin_active($plugin) ?>

<?php
    require_once get_stylesheet_directory()."/app/facturas.php";
    
    //Peticiones POST
    $correo = $_POST['correo'];
    $numero_factura = $_POST['numero-factura'];

    $wp_query_facturas = new wp_query_facturas();
    $paguelo_facil = new pago_API($wp_query_facturas);

    if(isset($correo)){
          $wp_query_facturas->wp_query_cliente($correo);
          $wp_query_facturas->wp_query_facturas($numero_factura);

          $tabla_facturas = new tabla_facturas($wp_query_facturas);
          $tabla_facturas->encolar_tabla();

          $paguelo_facil->suministrar_datos_transaccion('40BE8A3E47B922E22A47B6FA128A0766B3EBDFA821BDCDACBFAC62A7A35AC3AE',
          $tabla_facturas->get_table_data('total'), $paguelo_facil->descripcion_facturas_pendientes());
          
?>
        <div class="btn-pago">
            <?php $tabla_facturas->get_table_data('tabla_mostrada')? 
            $paguelo_facil->imprimir() : ''; ?>
        </div>

<?php
    }

    //Peticiones GET
     $pago = $_GET['pago'];
     $user_id = $_GET['user'];
     if(isset($pago) && isset($user_id)){
        $paguelo_facil->pago_realizado($pago, $user_id);
     }

     /*$ref = $_GET['ref'];
     if(isset($ref)){
        $wp_query_facturas->wp_query_transacciones($ref);

         $tabla_transacciones = new tabla_transacciones($wp_query_facturas);
         $tabla_transacciones->encolar_tabla();
     }*/
?>
    </div>


<?php get_footer() ?>

