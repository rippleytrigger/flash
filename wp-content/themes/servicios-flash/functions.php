<?php

/* ---------------------------------------------------------------------------
 * Child Theme URI | DO NOT CHANGE
 * --------------------------------------------------------------------------- */
define( 'CHILD_THEME_URI', get_stylesheet_directory_uri() );


/* ---------------------------------------------------------------------------
 * Define | YOU CAN CHANGE THESE
 * --------------------------------------------------------------------------- */

// White Label --------------------------------------------
define( 'WHITE_LABEL', false );

// Static CSS is placed in Child Theme directory ----------
define( 'STATIC_IN_CHILD', false );


/* ---------------------------------------------------------------------------
 * Enqueue Style
 * --------------------------------------------------------------------------- */
function add_my_styles() {
	
	// Enqueue the parent stylesheet
// 	wp_enqueue_style( 'parent-style', get_template_directory_uri() .'/style.css' );		
//we don't need this if it's empty
	
	// Enqueue the parent rtl stylesheet
	if ( is_rtl() ) {
		wp_enqueue_style( 'bridge-rtl', get_template_directory_uri() . '/rtl.css' );
	}
	
	// Enqueue the child stylesheet
	wp_dequeue_style( 'style' );
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() .'/style.css' );
	
	// Enqueue Logoscorp CSS
	wp_dequeue_style( 'main' );
	wp_enqueue_style( 'main', get_stylesheet_directory_uri() .'/css/main.css' );
	
}
add_action( 'wp_enqueue_scripts', 'add_my_styles', 101 );

/* ---------------------------------------------------------------------------
 * RIPPLEY STUFFS
 * --------------------------------------------------------------------------- */
// Hide Admin Bar in frontend
show_admin_bar(false);

// Load my JS Scripts by @rippleytrigger
function add_my_scripts() {

	wp_enqueue_script(
		'main-scripts',
		get_stylesheet_directory_uri() . '/js/main.js',
		array('jquery')
	);
}

// Add my script to Wordpress
add_action('wp_enqueue_scripts', 'add_my_scripts');

// Add Logoscorp class to the body of all pages
add_filter( 'body_class','my_body_classes' );
function my_body_classes( $classes ) {

	$classes[] = 'rippley';

	return $classes;

}


/* ---------------------------------------------------------------------------
 * Load Textdomain
 * --------------------------------------------------------------------------- */
add_action( 'after_setup_theme', 'flash_textdomain' );
function flash_textdomain() {
    load_child_theme_textdomain( 'flash',  get_stylesheet_directory() . '/languages' );
}


/* ---------------------------------------------------------------------------
 * Override theme functions
 * 
 * if you want to override theme functions use the example below
 * --------------------------------------------------------------------------- */
// require_once( get_stylesheet_directory() .'/includes/content-portfolio.php' );

if (!function_exists('qode_register_menus')) {
	/**
	 * Function that registers menu positions
	 */

	add_action( 'after_setup_theme', 'qode_register_menus' );
	function qode_register_menus() {
		global $qode_options_proya;

		if((isset($qode_options_proya['header_bottom_appearance']) && $qode_options_proya['header_bottom_appearance'] != "stick_with_left_right_menu") || (isset($qode_options_proya['vertical_area']) && $qode_options_proya['vertical_area'] == "yes")){
			register_nav_menus(
				array('top-navigation-left' => __( 'Top Navigation Left', 'qode')
				)
			);
			register_nav_menus(
				array('top-navigation-right' => __( 'Top Navigation Right', 'qode')
				)
			);
			register_nav_menus(
				array('top-navigation-cms-facturacion' => __( 'Top Navigation CMS', 'qode')
				)
			);
		}

		//popup menu location
		register_nav_menus(
			array('popup-navigation' => __( 'Fullscreen Navigation', 'qode')
			)
		);

		if((isset($qode_options_proya['header_bottom_appearance']) && $qode_options_proya['header_bottom_appearance'] == "stick_with_left_right_menu") && (isset($qode_options_proya['vertical_area']) && $qode_options_proya['vertical_area'] == "no")){
			//header left menu location
			register_nav_menus(
				array('left-top-navigation' => __( 'Left Top Navigation', 'qode')
				)
			);

			//header right menu location
			register_nav_menus(
				array('right-top-navigation' => __( 'Right Top Navigation', 'qode')
				)
			);
			
		}
	}

	add_action( 'after_setup_theme', 'register_custom_sidebars' );
	function register_custom_sidebars(){
		register_sidebar(array(
			'name' => 'Side Area CMS',
			'id' => 'sidearea_cms',
			'description' => 'Side Area CMS',
			'before_widget' => '<div id="%1$s" class="widget %2$s posts_holder">',
			'after_widget' => '</div>',
			'before_title' => '<h5>',
			'after_title' => '</h5>'
		));
	}
}


// Replace the include_blank string in CF7
function my_wpcf7_form_elements($html) {
	function rippley_replace_include_blank($name, $text, &$html) {
	$matches = false;
	preg_match('/<select name="' . $name . '"[^>]*>(.*)<\/select>/iU', $html, $matches);
		if ($matches) {
		$select = str_replace('<option value="">---</option>', '<option value="">' . $text . '</option>', 
		$matches[0]);
		$html = preg_replace('/<select name="' . $name . '"[^>]*>(.*)<\/select>/iU', $select, $html);
		}
	}
	rippley_replace_include_blank('servicios', 'Servicios', $html);
	return $html;
}
	add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');


// define the wp_mail_failed callback 
function action_wp_mail_failed($wp_error) 
{
    return error_log(print_r($wp_error, true));
}
          
// add the action 
add_action('wp_mail_failed', 'action_wp_mail_failed', 10, 1);